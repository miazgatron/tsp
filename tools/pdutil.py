import os
import re
from os.path import splitext, join

import pandas as pd


def read_all_csv(path):
    dfs = []
    for i, file in enumerate(os.listdir(path)):
        df = pd.read_csv(join(path, file))
        df.insert(0, 'launch_id', i)
        name, algo, cyc, sig, seed = splitext(file)[0].split('_')
        df['n'] = int(re.sub(r'\D', '', name))
        df['input'] = name
        df['algorithm'] = {'naive': 'Naive', 'deberg': 'dBBJW', 'clever': 'CKS', 'combined': 'Combined'}[algo]
        df['initial_cycle'] = cyc
        df['shuffle_signatures'] = sig == 'sigshuf'
        df['seed'] = int(seed)
        dfs.append(df)
    return pd.concat(dfs, ignore_index=True)
