#!/usr/bin/env python3

import logging
import os
from logging import critical, info
from math import ceil

from old_test import chdir_project_root, get_solver


def globalcmp_single(testname):
    chdir_project_root()
    with open(f'global/{testname}') as f:
        input = f.read()
    with open(f'exp/{testname}.csv', 'w') as csv:
        print('k,algo,exp,sig,change,time,step,weight,run', file=csv)
        deadline = 0
        step = 30
        for run in 'combined', 'naive', 'clever', 'deberg':
            solver = get_solver(cpu_limit=None)
            args = ['--global', f'--algorithm={run}',
                    f'--deadline={deadline}' if deadline else f'--deadline-step={step}']
            info(args)
            proc = solver(input, args)
            if proc.status != 0:
                critical(f'Process exited with status {proc.status}')
            data = [line.split() for line in proc.stdout.splitlines()[:-2]]
            if not deadline:
                deadline = ceil(int(data[-1][5]) / 10**6) + step
            for line in data:
                print(*line, run, sep=',', file=csv)
            csv.flush()


def globalcmp():
    for test in os.listdir('global'):
        info(f'Running {test}')
        globalcmp_single(test)


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    globalcmp()
