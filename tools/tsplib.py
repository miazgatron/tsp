#!/usr/bin/env python3
from glob import glob
from math import sqrt
from os.path import join, basename

from old_test import Graph


def read_tsp_graph(tsp_file):
    for _ in range(6):
        next(tsp_file)
    data = []
    while True:
        line = next(tsp_file)
        if line.find('EOF') != -1:
            break
        try:
            _, x, y = map(float, line.split())
        except ValueError as e:
            raise ValueError(f'Error in line "{line}": {e}') from None
        data.append((x, y))

    n = len(data)
    g = Graph(n)
    for i in range(1, n):
        x1, y1 = data[i]
        for j in range(i):
            x2, y2 = data[j]
            g[i, j] = round(sqrt((x1 - x2)**2 + (y1 - y2)**2))
    return g


def read_tsp_tour(tsp_file):
    for _ in range(5):
        next(tsp_file)
    data = []
    while True:
        line = next(tsp_file)
        if line.find('EOF') != -1:
            break
        data.extend(int(x) - 1 for x in line.split())
    return data


def convert(dir, name):
    with open(join(dir, f'{name}.tsp')) as f:
        graph = read_tsp_graph(f)
    with open(join(dir, f'{name}.opt.tour')) as f:
        tour = read_tsp_tour(f)
    with open(join(dir, name), 'x') as f:
        print(graph, ' '.join(map(str, tour)), file=f)


def convert_all(dir):
    for name in glob(join(dir, '*.tsp')):
        name = basename(name)[:-4]
        convert(dir, name)
