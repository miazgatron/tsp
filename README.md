### Decomposer
For the `decompose.py` to work, a command `tw-exact` must be available
as specified [here](https://pacechallenge.wordpress.com/pace-2017/track-a-treewidth/).
The following script should be enough to get it working on a Debian system:
```sh
sudo apt-get install autotools-dev automake libtool libboost-graph-dev stx-btree-dev libboost-thread-dev
git clone https://github.com/freetdi/p17.git larish
cd larish && make && cd -
cat >tw-exact <<EOF
#!/bin/sh
cd '$(readlink -f larish)' && exec ./tw-exact "\$@"
EOF
chmod +x tw-exact
```
