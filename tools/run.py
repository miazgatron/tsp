#!/usr/bin/env python3
from argparse import ArgumentParser
from os import mkdir

from old_test import chdir_project_root, get_solver, Input


def parse_args(args=None):
    parser = ArgumentParser()
    parser.add_argument('--name', default='kopt')
    parser.add_argument('--sig', action='store_true', default=False, help='print signature statistics')
    parser.add_argument('k', type=int, help='size of k-move')
    parser.add_argument('n', type=int, help='number of vertices')
    return parser.parse_args(args)


def binom(n, k):
    enum, denom = 1, 1
    for i in range(k):
        enum *= n - i
        denom *= k - i
    return enum // denom


def complexity(n, bags):
    return sum(num * binom(n, k) for k, num in enumerate(bags))


def signature_stats(n, stderr: str):
    mxbag = 4
    series = [[] for _ in range(mxbag + 1)]
    for line in stderr.splitlines():
        sig, time, *bags = line.split()
        time = float(time)
        bags = [int(b) for b in bags]
        series[len(bags) - 1].append((complexity(n, bags), time))
    for k in range(1, mxbag + 1):
        with open(f'run/sig{k}', 'a') as f:
            for arg, val in series[k]:
                print(arg, val, file=f)


def main(args):
    chdir_project_root()
    try:
        mkdir('run')
    except FileExistsError:
        pass

    data = f'{Input.generate(args.k, args.n)}\n'
    proc = get_solver(name=args.name, cpu_limit=None)(data)
    status = f'Finished with status {proc.status} after {proc.time.system + proc.time.user} s.'
    print(status)
    with open('run/ret', 'w') as f:
        print(status, file=f)
    with open('run/in', 'w') as stdin:
        stdin.write(data)
    with open('run/out', 'w') as stdout:
        stdout.write(proc.stdout)
    with open('run/err', 'w') as stderr:
        stderr.write(proc.stderr)
    if args.sig:
        signature_stats(args.n, proc.stderr)


if __name__ == '__main__':
    main(parse_args())
