#!/usr/bin/env python3
"""This module contains a demo of efficient iteration over all k-subsets of an n-set."""


def binom(n, k):
    assert n >= 0 and k >= 0
    enum, denom = 1, 1
    for i in range(k):
        enum *= n - i
        denom *= k - i
    return enum // denom


class Subset:
    def __init__(self, n, k):
        assert 0 <= k <= n
        self.n = n
        self.k = k
        self.a = [0] * (k + 1)
        self.b = list(reversed(range(k + 1)))
        self.x = list(range(k)) + [n]

    def next(self):
        i = 0
        while i < self.k and self.x[i] + 1 >= self.x[i + 1]:
            i += 1
        if i < self.k:
            self.a[i] += binom(self.x[i], i)
            self.b[i] += binom(self.x[i], i - 1) if i > 0 else 0
            self.x[i] += 1
        for j in range(i - 1, -1, -1):
            self.a[j] = self.a[j + 1]
            self.b[j] = self.b[j + 1] + 1
            self.x[j] = j
        return i < self.k

    def index(self, exclude=None):
        if exclude is None:
            return self.a[0]
        else:
            return self.a[0] - self.a[exclude] + self.b[exclude + 1]


def test_iteration(n, k):
    s = Subset(n, k)
    for i in range(0, binom(n, k) - 1):
        assert s.index() == i
        assert s.next()
    assert s.index() == binom(n, k) - 1
    assert not s.next()
    assert s.index() == 0


def test_exclusion(n, k):
    s = Subset(n, k - 1)
    idx = {tuple(s.x): 0}
    while s.next():
        idx[tuple(s.x)] = s.index()

    s = Subset(n, k)
    for i in range(k):
        assert s.index(exclude=i) == idx[tuple(s.x[:i] + s.x[i + 1:])]
