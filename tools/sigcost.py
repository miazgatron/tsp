#!/usr/bin/env python3
import logging
from logging import info

from old_test import chdir_project_root, get_solver, Input


def parse_results(err: str, data: dict):
    for line in err.splitlines():
        arg, val = map(int, line.split())
        data.setdefault(arg, []).append(val)


def print_results(data: dict):
    with open('results', 'w') as f:
        for arg, val in data.items():
            avg = round(sum(val) / len(val))
            print(f'{arg},{min(val)},{avg},{max(val)}', file=f)


def main():
    logging.basicConfig(level=logging.INFO)
    chdir_project_root()
    solver = get_solver(cpu_limit=None)

    data = {}
    runs = (
        (4, range(10, 150 + 1, 5)),
        (5, range(10, 70 + 1, 5)),
        (6, range(10, 30 + 1, 5)),
        (7, range(10, 15 + 1, 1)),
    )
    for k, sizes in runs:
        for n in sizes:
            proc = solver(str(Input.generate(k, n)))
            info(f'Finished ({k}, {n}) with status {proc.status} after {proc.time.system + proc.time.user} s.')
            parse_results(proc.stderr, data)
    print_results(data)


if __name__ == '__main__':
    main()
