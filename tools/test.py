#!/usr/bin/env python3
import argparse
import logging
import subprocess
from subprocess import DEVNULL
from tempfile import NamedTemporaryFile

import tsplib95

import tools
from tools import get_tests, make

log = logging.getLogger()


def parse_args():
    parser = argparse.ArgumentParser('Run simple validity tests')
    parser.add_argument('-v', '--verbose', action='count', default=0, help='be more verbose')
    parser.add_argument('-q', '--quiet', action='count', default=0, help='be more quiet')
    # parser.add_argument('--min-k', metavar='NUM', type=int, help='minimum k to run tests with')
    # parser.add_argument('--max-k', metavar='NUM', type=int, help='maximum k to run tests with')
    # parser.add_argument('--k', metavar='NUM', type=int, help='shortcut for setting --min-k and --max-k to one value')
    args, solver_args = parser.parse_known_args()
    log_level = logging.WARNING + 10 * (args.quiet - args.verbose)
    logging.basicConfig(level=log_level)
    return solver_args


def get_initial_weight(problem):
    sol = tsplib95.Solution()
    sol.tours = [list(range(1, problem.dimension + 1))]
    return problem.trace_tours(sol)[0]


def get_best(problem, solution):
    """Returns a list l such that l[k] is the best cycle that can be achieved with a single k-move"""
    best = [None, get_initial_weight(problem)] + problem.trace_tours(solution)
    for k in range(2, len(best)):
        best[k] = min(best[k - 1], best[k])
    return best


def run_test(args, test):
    log.info(f'[{test.name}] Running test...')
    problem = test.problem()
    best = get_best(problem, test.solution())

    min_k, max_k = 2, 4
    with NamedTemporaryFile('w+') as output:
        args = args + [f'--min-k={min_k}', f'--max-k={max_k}', f'--input={test.tsp_path}']
        proc = subprocess.run(args, cwd=tools.prefix, stdin=DEVNULL, stdout=output)
        if proc.returncode:
            log.error(f'[{test.name}] Solver exited with status {proc.returncode}')
            return False
        solution = tsplib95.load_solution(output.name)

    for k, weight in zip(range(min_k, max_k + 1), problem.trace_tours(solution)):
        if weight != best[k]:
            log.error(f'[{test.name}] Got weight {weight}, expected {best[k]} for k = {k}')
            return False

    return True


def main(solver_args):
    tests = get_tests('gen/*')
    passed = 0
    failed = []
    with make() as solver:
        solver_args = [solver.path] + solver_args
        for test in tests:
            if run_test(solver_args, test):
                passed += 1
            else:
                failed.append(test.name)

    print(f'Passed {passed}/{len(tests)} tests.')
    if failed:
        failed = ' '.join(failed)
        print(f'Failed: {failed}.')


if __name__ == '__main__':
    try:
        main(parse_args())
    except KeyboardInterrupt:
        pass
