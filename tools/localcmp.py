#!/usr/bin/env python3
import logging
import itertools
import random
import subprocess
import sys
from contextlib import contextmanager, ExitStack
from random import randrange
from statistics import mean
from subprocess import DEVNULL, Popen
from tempfile import NamedTemporaryFile

grid_size = {2: 1000, 3: 100, 4: 20, 5: 5, 6: 2, 7: 1}
timeout = {2: 5}

log = logging.getLogger()


def main():
    logging.basicConfig(level=logging.INFO)
    random.seed(tuple(sys.argv))
    k = int(sys.argv[1])
    algorithm = sys.argv[2]
    if algorithm == 'hardcoded' and k not in (2, 3):
        log.warning(f'Skipping hardcoded for k = {k}')
        return
    run(k, algorithm)


def run(k, algorithm):
    with open(f'local/{k}_{algorithm}.csv', 'w') as f:
        print('k,algorithm,n,time', file=f)
        for n in grid(k):
            time = get_time(algorithm, n, k)
            print(f'{k},{algorithm},{n},{time}', file=f)
            if time > timeout.get(k, 30):
                break


def get_time(algorithm, n, k, *, nproc=3):
    log.info(f'Running {algorithm} with n = {n}, k = {k}, nproc = {nproc}')
    proc = []
    with ExitStack() as stack:
        for _ in range(nproc):
            test = stack.enter_context(gen_test(n))
            args = ['./kopt', '--library=data', f'--algorithm={algorithm}', f'--k={k}', f'--input={test}']
            proc.append(stack.enter_context(time_popen(args, stdout=DEVNULL)))
    for p in proc:
        if p.returncode:
            cmd = f"'{' '.join(p.args)}'"
            log.error(f'{cmd} returned with status {p.returncode}')
    time = mean(p.time for p in proc)
    log.info(f'Finished in time {time}')
    return time


@contextmanager
def gen_test(n):
    with NamedTemporaryFile() as tmp:
        subprocess.run(('./gentest', f'--seed={randrange(1000)}', f'--n={n}', f'--tsp-file={tmp.name}'), check=True)
        yield tmp.name


@contextmanager
def time_popen(cmd, *args, **kwargs):
    with NamedTemporaryFile('r') as tmp:
        cmd = ['time', '--output', tmp.name, '--format', '%x %U %S', '--'] + cmd
        with Popen(cmd, *args, **kwargs) as proc:
            yield proc
        # `time` can output a warning message; the results are in the last line.
        out = tmp.readlines()[-1].split()
        proc.returncode = int(out[0])
        proc.time = float(out[1]) + float(out[2])


def grid(k):
    add = grid_size[k]
    yield k
    yield from itertools.count(k - k % add + add, add)


if __name__ == '__main__':
    main()
