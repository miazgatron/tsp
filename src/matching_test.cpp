#include <matching.h>

#include <gtest/gtest.h>

namespace kopt {
namespace {

std::vector<int> ToVector(const Matching &matching) {
  std::vector<int> vec(static_cast<size_t>(matching.Domain().Size()));
  for (ModNode x : matching.Domain())
    vec[x.Id() - 1] = matching(x).Id();
  return vec;
}

void VerifyIteration(Matching &matching, const std::vector<std::vector<int>> &expected) {
  for (unsigned i = 0; i < expected.size(); ++i) {
    EXPECT_EQ(ToVector(matching), expected[i]);
    if (i + 1 == expected.size())
      ASSERT_FALSE(matching.Next());
    else
      ASSERT_TRUE(matching.Next());
  }
}

TEST(Matching, Domain) {
  Matching m(4);
  for (int i = 1; i <= 8; ++i)
    EXPECT_TRUE(m.Domain().Contains(ModNode(i)));
  EXPECT_FALSE(m.Domain().Contains(ModNode(9)));
}

TEST(Matching, Values) {
  Matching m({3, 4, 2, 1});
  EXPECT_EQ(m(ModNode(1)), ModNode(3));
  EXPECT_EQ(m(ModNode(2)), ModNode(4));
  EXPECT_EQ(m(ModNode(3)), ModNode(2));
  EXPECT_EQ(m(ModNode(4)), ModNode(1));
}

TEST(Matching, IterationK1) {
  Matching m(1);
  VerifyIteration(m, {
      {2, 1},
  });
}

TEST(Matching, IterationK2) {
  Matching m(2);
  VerifyIteration(m, {
      {2, 1, 4, 3},
      {3, 4, 1, 2},
  });
}

TEST(Matching, IterationK3) {
  Matching matching(3);
  VerifyIteration(matching, {
      {2, 1, 4, 3, 6, 5},
      {2, 1, 5, 6, 3, 4},
      {3, 4, 1, 2, 6, 5},
      {3, 5, 1, 6, 2, 4},
      {4, 5, 6, 1, 2, 3},
      {4, 6, 5, 1, 3, 2},
      {5, 4, 6, 2, 1, 3},
      {5, 6, 4, 3, 1, 2},
  });
}

TEST(Matching, Output) {
  std::ostringstream output;
  output << Matching({3, 4, 1, 2, 6, 5});
  EXPECT_EQ(output.str(), "{{1, 3}, {2, 4}, {5, 6}}");
}

}  // namespace
}  // namespace kopt
