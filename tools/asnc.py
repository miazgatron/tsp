from asyncio import create_subprocess_exec, Queue, create_task, gather, CancelledError, create_subprocess_shell, sleep


async def solver(timeout):
    print(f'solver({timeout})')
    await sleep(timeout)
    print(f'done {timeout}')


async def stress():
    proc = await create_subprocess_shell('while :; do :; done')
    try:
        await proc.wait()
    finally:
        proc.kill()


async def worker(queue):
    while True:
        coro = await queue.get()
        if coro is None:
            queue.task_done()
            await stress()
        else:
            await coro
            queue.task_done()


async def run_all(coros, nproc=8):
    queue = Queue()
    for coro in coros:
        queue.put_nowait(coro)
    for _ in range(nproc - 1):
        queue.put_nowait(None)
    tasks = [create_task(worker(queue)) for _ in range(nproc)]
    await queue.join()
    for task in tasks:
        task.cancel()
    await gather(*tasks, return_exceptions=True)
