#!/usr/bin/env python3
import itertools
import logging
import os
import subprocess
from multiprocessing import cpu_count
from multiprocessing.dummy import Pool
from os.path import join, basename, splitext, exists
from subprocess import DEVNULL
from tempfile import NamedTemporaryFile

from tsplib_meta import test_meta

log = logging.getLogger()

KILL = object()

prefix = os.getcwd()
solver_path = join(prefix, 'kopt')
tests_path = join(prefix, 'tests')
library_path = join(prefix, 'data')
output_path = join(prefix, 'output')


def main():
    logging.basicConfig(level=logging.INFO)
    try:
        multirun(gen_args())
    except KeyboardInterrupt:
        pass


def gen_args():
    prod = product(
        input=get_tests(),
        algorithm=('combined', 'deberg', 'clever', 'naive'),
        seed=range(10),
    )
    for args in prod:
        args.update(
            iterate=True,
            library=library_path,
            shuffle_signatures=True,
            initial_cycle='shuffle',
            # deadline_step=30,
            deadline=test_meta[test_name(args['input'])].deadline,
        )
        yield args


def multirun(tests):
    tests = list(tests)
    n = len(tests)
    nproc = cpu_count()
    log.info(f'Starting {nproc} runners for {n} tests')
    tests = list((f'{i + 1}/{n}', t) for i, t in enumerate(tests)) + [None] * (nproc - 1) + [KILL]
    with Pool(nproc) as pool:
        return pool.map(runner, tests, chunksize=1)


def runner(args):
    if args is None:
        log.info('Start stressing')
        subprocess.run(['stress'])
        log.info('Done stressing')
    elif args is KILL:
        log.info('Killing runners')
        subprocess.run(['pkill', 'stress'])
        log.info('Killed runners')
    else:
        run_solver(*args)


def run_solver(num, args):
    output = output_file(args)
    if output is None:
        return
    with NamedTemporaryFile(dir=output_path, prefix='', suffix='.tmp') as tmp:
        args = [solver_path] + list(to_flags(**args))
        cmd = ' '.join(args)
        proc = subprocess.run(args, stdin=DEVNULL, stdout=tmp, stderr=DEVNULL)
        if proc.returncode:
            log.error(f'{cmd} exited with {proc.returncode}')
        else:
            os.link(tmp.name, output)
    log.info(f'Run {num} finished')


def output_file(args):
    name = '_'.join(map(str, (
        test_name(args['input']),
        args['algorithm'],
        args['initial_cycle'],
        'sigshuf' if args['shuffle_signatures'] else 'sigfixed',
        args['seed'],
    )))
    path = join(output_path, name + '.csv')
    if exists(path):
        log.warning(f'Skip {name}')
        return None
    else:
        log.info(f'Start {name}')
        return path


def get_tests():
    return (join(tests_path, test) for test in os.listdir(tests_path))


def test_name(path):
    return splitext(basename(path))[0]


def product(**kwargs):
    for values in itertools.product(*kwargs.values()):
        yield dict(zip(kwargs.keys(), values))


def to_flags(**kwargs):
    for key, val in kwargs.items():
        key = key.replace('_', '-')
        if val is None:
            pass
        elif isinstance(val, bool):
            yield f'--{key}' if val else f'--no{key}'
        else:
            yield f'--{key}'
            yield str(val)


if __name__ == '__main__':
    main()
