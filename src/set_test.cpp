#include <set.h>

#include <limits>

#include <gtest/gtest.h>

namespace kopt {
namespace {

TEST(Set, DefaultConstructor) {
  Set<ModEdge> set;
  for (int i = 1; i <= std::numeric_limits<int>::digits; ++i)
    ASSERT_FALSE(set.Contains(ModEdge(i)));
}

TEST(Set, SingleElementConstructor) {
  Set<ModEdge> set(ModEdge(5));
  ASSERT_TRUE(set.Contains(ModEdge(5)));
  for (int i = 1; i <= std::numeric_limits<int>::digits; ++i) {
    if (i != 5)
      ASSERT_FALSE(set.Contains(ModEdge(i)));
  }
}

TEST(Set, Iterate) {
  std::vector<int> expected{2, 4, 5}, actual;
  for (auto x : Set<ModEdge>(expected))
    actual.push_back(x.Id());
  EXPECT_EQ(actual, expected);
}

}  // namespace
}  // namespace kopt
