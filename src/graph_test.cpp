#include <graph.h>

#include <algorithm>
#include <sstream>

#include <gtest/gtest.h>

namespace kopt {

TEST(Graph, Cycle) {
  std::istringstream input(
      "4 4\n"
      "1 2 11\n"
      "1 4 14\n"
      "2 3 12\n"
      "3 4 13\n");
  std::ostringstream output;

  Graph graph;
  input >> graph;
  output << graph;
  EXPECT_EQ(input.str(), output.str());

  EXPECT_EQ(graph.Weight(Id(1)), Int(11));
  EXPECT_EQ(graph.Weight(Id(2)), Int(12));
  EXPECT_EQ(graph.Weight(Id(3)), Int(13));
  EXPECT_EQ(graph.Weight(Id(4)), Int(14));

  const int weight[4][4] = {
      {0, 11, 0, 14},
      {11, 0, 12, 0},
      {0, 12, 0, 13},
      {14, 0, 13, 0},
  };
  for (int i = 1; i <= 4; ++i) {
    for (int j = 1; j <= 4; ++j) {
      Int expected = weight[i-1][j-1] ? Int(weight[i-1][j-1]) : Int::Inf();
      EXPECT_EQ(graph.Weight(Id(i), Id(j)), expected) << "i = " << i << ", j = " << j;
    }
  }
}

TEST(Graph, Clique) {
  std::istringstream input(
      "5 10\n"
      "1 2 2\n"
      "1 3 1\n"
      "1 4 2\n"
      "1 5 1\n"
      "2 3 1\n"
      "2 4 1\n"
      "2 5 2\n"
      "3 4 2\n"
      "3 5 2\n"
      "4 5 1\n");
  std::ostringstream output;

  Graph graph;
  input >> graph;
  output << graph;
  EXPECT_EQ(input.str(), output.str());
  EXPECT_EQ(graph.CycleWeight(Ids{1, 3, 2, 4, 5}), Int(5));
}

}  // namespace kopt
