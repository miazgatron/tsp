#!/usr/bin/env python3
from argparse import ArgumentParser
from logging import info

import logging
import math

from old_test import chdir_project_root, get_solver, Input, Graph


def parse_args(args=None):
    parser = ArgumentParser()
    parser.add_argument('k', type=int, help='size of k-move')
    parser.add_argument('min', type=int, help='minimum size of graph')
    parser.add_argument('max', type=int, help='maximum size of graph')
    parser.add_argument('--grid', type=int, default=30, help='number of data points')
    return parser.parse_args(args)


class Stats:
    cols = ('L0', 'I1', 'I2', 'I3', 'I4', 'F0', 'F1', 'F2', 'F3')

    def __init__(self):
        self.rows = []

    def add(self, n, stderr: str):
        row_sum = [0] * len(Stats.cols)
        row_num = [0] * len(Stats.cols)
        *lines, summary = stderr.splitlines()
        for line in lines:
            char, size, time = line.split()
            idx = Stats.cols.index(char + size)
            row_sum[idx] += int(time)
            row_num[idx] += 1
        self.rows.append([n] + [round(x / y) for x, y in zip(row_sum, row_num)])
        total_time, clocks_per_s = map(int, summary.split())
        ratio = round(sum(row_sum) / total_time, 2)
        seconds = round(sum(row_sum) / clocks_per_s, 3)
        info(f'Vertex processing took {seconds} s ({100 * ratio} % of total time)')

    def print(self):
        with open('run/vstats', 'w') as f:
            print(','.join(('n',) + Stats.cols), file=f)
            for row in self.rows:
                print(','.join(map(str, row)), file=f)


def main(args):
    logging.basicConfig(level=logging.INFO)
    chdir_project_root()
    solver = get_solver(cpu_limit=30)

    base = math.exp(math.log(args.max / args.min) / (args.grid - 1))
    stats = Stats()
    for p in range(args.grid):
        n = round(args.min * pow(base, p))
        data = str(Input(args.k, Graph.generate(n)))
        proc = solver(data)
        time = proc.time.system + proc.time.user
        info(f'Finished n = {n} with status {proc.status} after {time} s.')
        if proc.status == 0:
            stats.add(n, proc.stderr)
        else:
            break
    stats.print()


if __name__ == '__main__':
    main(parse_args())
