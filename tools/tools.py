import logging as log
import os
import subprocess
import atexit
from contextlib import contextmanager
from dataclasses import dataclass
from glob import glob
from io import StringIO
from itertools import product
from os.path import abspath, dirname, join, relpath, isfile, splitext
from shutil import copy2
from subprocess import DEVNULL, PIPE, Popen
from tempfile import mkstemp, NamedTemporaryFile

import asyncio

import tsplib95
from pandas import read_csv, DataFrame, Series

prefix = abspath(join(dirname(__file__), '..'))


def prod(**kwargs):
    for values in product(*kwargs.values()):
        yield dict(zip(kwargs.keys(), values))


def to_flags(**kwargs):
    for key, val in kwargs.items():
        key = key.replace('_', '-')
        if val is None:
            pass
        elif isinstance(val, bool):
            yield f'--{key}' if val else f'--no-{key}'
        else:
            yield f'--{key}'
            yield str(val)


class PathToRemove:
    def __init__(self, path):
        self.path = path
        atexit.register(self.close)

    def close(self):
        if self.path is not None:
            try:
                os.remove(self.path)
            except FileNotFoundError:
                log.warning(f"File '{self.path}' already removed")
            self.path = None
            atexit.unregister(self.close)

    def __enter__(self):
        return self

    def __exit__(self, *_):
        self.close()
        return False


def make(target='kopt', config='release'):
    build_dir = join(prefix, 'build', config)
    subprocess.run(['make', target], cwd=build_dir, stdin=DEVNULL, stdout=DEVNULL, check=True)
    fd, path = mkstemp(prefix=f'{target}.', dir=build_dir)
    os.close(fd)
    copy2(join(build_dir, target), path)
    return PathToRemove(path)


@contextmanager
def time_popen(cmd, *args, **kwargs):
    with NamedTemporaryFile('r') as tmp:
        cmd = ['time', '--output', tmp.name, '--format', '%x %U %S', '--'] + cmd
        with Popen(cmd, *args, **kwargs) as proc:
            yield proc
        # `time` can output a warning message; the results are in the last line.
        out = tmp.readlines()[-1].split()
        proc.returncode = int(out[0])
        proc.time = float(out[1]) + float(out[2])


@contextmanager
def gen_test(n):
    with NamedTemporaryFile(suffix='.tsp') as tmp:
        with make('gentest') as gen:
            subprocess.run((gen.path, f'--n={n}', f'--tsp-file={tmp.name}'), stdin=DEVNULL, stdout=DEVNULL, check=True)
        yield tmp.name


@dataclass(order=True, frozen=True)
class Test:
    dir: str
    name: str

    @property
    def tsp_path(self) -> str:
        return join(self.dir, self.name) + '.tsp'

    @property
    def tour_path(self) -> str:
        return join(self.dir, self.name) + '.tour'

    def problem(self):
        return tsplib95.load_problem(self.tsp_path)

    def solution(self):
        return tsplib95.load_solution(self.tour_path)


def get_tests(*patterns):
    tests_dir = join(prefix, 'tests')
    tests = []
    for pattern in patterns:
        matched = [path for path in glob(join(tests_dir, pattern) + '.tsp', recursive=True) if isfile(path)]
        if not matched:
            log.warning(f"Pattern '{pattern}' did not match any files")
            continue
        matched.sort()
        for path in matched:
            name = splitext(relpath(path, tests_dir))[0]
            tests.append(Test(tests_dir, name))
    return tests


def get_test(pattern):
    res = get_tests(pattern)
    if len(res) > 1:
        log.warning(f"Pattern '{pattern}' matched {len(res)} files")
    return res[0] if res else None


@dataclass
class LaunchParams:
    test: Test
    algorithm: str = 'combined'
    initial_cycle: str = 'shuffle'
    seed: int = 0
    deadline: int = 0
    deadline_step: int = 0
    # For CSV only
    id: int = None
    tag: str = None
    ret: int = None
    # For flags only
    iterate: bool = True
    library: str = join(prefix, 'data/decomposition')

    def to_flags(self):
        return list(to_flags(
            iterate=self.iterate, library=self.library,
            input=self.test.tsp_path, algorithm=self.algorithm, initial_cycle=self.initial_cycle, seed=self.seed,
            deadline=self.deadline, deadline_step=self.deadline_step
        ))

    def to_df(self):
        return DataFrame([dict(
            id=self.id, tag=self.tag, ret=self.ret,
            input=self.test.name, algorithm=self.algorithm, initial_cycle=self.initial_cycle, seed=self.seed,
            deadline=self.deadline, deadline_step=self.deadline_step
        )])


def get_csv(csv_name):
    return read_csv(join(prefix, 'data', csv_name))


def write_csv(csv_name, df: DataFrame, *, truncate=False):
    with open(join(prefix, 'data', csv_name), 'w' if truncate else 'a') as f:
        df.to_csv(f, index=False, header=f.tell() == 0)


def delete_launch(run_id):
    runs = get_csv('launches.csv')
    steps = get_csv('steps.csv')
    write_csv('launches.csv', runs[runs.id != run_id], truncate=True)
    write_csv('steps.csv', steps[steps.run_id != run_id], truncate=True)


def next_id(csv_name):
    try:
        df = read_csv(join(prefix, 'data', csv_name))
        return 0 if df.empty else df.id.max() + 1
    except FileNotFoundError:
        return 0


def get_opt(test_name):
    with open(get_test(test_name).tsp_path, 'r') as f:
        best, optimum = None, None
        for line in f:
            line = line.split()
            if line[0] == 'BEST':
                best = int(line[2])
            elif line[0] == 'OPTIMUM':
                optimum = bool(int(line[2]))
                return best, optimum


def get_data(tag=None, *, group=True):
    launches = get_csv('launches.csv').set_index('id')
    launches[['best', 'optimum']] = launches.apply(lambda row: Series(get_opt(row['input'])), axis=1)
    steps = get_csv('steps.csv')
    steps = steps.join(launches, on='launch_id')
    if tag is not None:
        steps = steps[steps['tag'] == tag]
    if group:
        steps = dict(tuple(steps.groupby('input')))
    return steps


async def experiment(path, params: LaunchParams):
    args = [path] + params.to_flags()
    log.info(' '.join(args))
    proc = await asyncio.subprocess.create_subprocess_exec(*args, stdin=DEVNULL, stdout=PIPE)
    stdout, _ = await proc.communicate()
    if proc.returncode != 0:
        log.error(f'Solver exited with status {proc.returncode}')
    params.id = next_id('launches.csv')
    params.ret = proc.returncode
    steps = read_csv(StringIO(stdout.decode('ascii')))
    steps.insert(0, 'launch_id', params.id)
    write_csv('launches.csv', params.to_df())
    write_csv('steps.csv', steps)
