#include <identifier.h>

#include <gtest/gtest.h>

namespace kopt {
namespace {

TEST(Identifier, ModNode) {
  EXPECT_FALSE(ModNode(4).IsLeft());
  EXPECT_TRUE(ModNode(4).IsRight());
  EXPECT_EQ(ModNode(4).Edge().Id(), 2);

  EXPECT_TRUE(ModNode(7).IsLeft());
  EXPECT_FALSE(ModNode(7).IsRight());
  EXPECT_EQ(ModNode(7).Edge().Id(), 4);
}

TEST(Identifier, ModEdge) {
  EXPECT_EQ(ModEdge(2).Left().Id(), 3);
  EXPECT_EQ(ModEdge(2).Right().Id(), 4);
  EXPECT_EQ(ModEdge(3).Left().Id(), 5);
  EXPECT_EQ(ModEdge(3).Right().Id(), 6);
}

}  // namespace
}  // namespace kopt
