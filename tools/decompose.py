#!/usr/bin/env python3
import logging
from bisect import bisect_left
from functools import total_ordering
from itertools import chain, permutations, groupby
from logging import info
from subprocess import Popen, PIPE, DEVNULL
from typing import Union


class CycleCover:
    def __init__(self, n, cycles):
        permutation = [None] * n
        for cycle in cycles:
            for i in range(len(cycle)):
                permutation[cycle[i-1]] = cycle[i]
        self.n = n
        self.permutation = permutation
        self.cycles = cycles

    def __repr__(self):
        return f'CycleCover({self.cycles!r})'


def cycle_covers(n, *, min_len=2):
    """Generates all covers of {0, ..., n-1} with undirected cycles."""
    assert n >= 0 and min_len >= 1
    for permutation in permutations(range(n)):
        cycles = []
        for x in permutation:
            if cycles and cycles[-1][0] > x:
                cycles[-1].append(x)
            else:
                cycles.append([x])
        if all(len(cycle) >= min_len and (len(cycle) == 1 or cycle[1] <= cycle[-1]) for cycle in cycles):
            yield CycleCover(n, cycles)


class Signature:
    def __init__(self, cycle):
        cycle = tuple(cycle)
        k = len(cycle) // 2
        matching = [None] * len(cycle)
        for i in range(k):
            x, y = cycle[2 * i], cycle[2 * i + 1]
            matching[x] = y
            matching[y] = x
        self.k = k
        self.cycle = cycle
        self.matching = tuple(matching)

    def __repr__(self):
        return f'Signature({self.cycle!r})'

    def changes(self):
        m = self.matching
        return sum(m[2 * i] != 2 * i + 1 for i in range(self.k))

    def de_berg_exponent(self):
        """Exponent in de Berg's algorithm for this signature."""
        k = self.k
        visited = [False] * k

        def step(x):
            visited[x // 2] = True
            x = self.matching[x]
            x = x - 1 if x % 2 else x + 1
            return x

        result = 1
        for endpoint in range(2 * k):
            length = 0
            while not visited[endpoint // 2]:
                endpoint = step(endpoint)
                length += 1
            result += (length + 1) // 2
        return result


def signatures(k, *, reducible=False):
    assert k >= 1
    p = list(range(k - 1))  # Permutation of the cycle pieces.
    o = [0] * (k - 1)  # Orientation of the cycle pieces.
    while True:
        cycle = (2 * p[i] + side for i in range(k - 1) for side in (1 + o[i], 2 - o[i]))
        signature = Signature(chain((0,), cycle, (2 * k - 1,)))
        if reducible or signature.changes() == k:
            yield signature

        for i in reversed(range(k - 1)):
            o[i] = 1 - o[i]
            if o[i]:
                break
            elif i + 1 < k - 1 and p[i] < p[i + 1]:
                low = next(j for j in range(k - 2, i, -1) if p[i] < p[j])
                p[i], p[low] = p[low], p[i]
                break
        else:
            break
        p[i + 1:] = reversed(p[i + 1:])


@total_ordering
class DependencyGraph:
    def __init__(self, arg: Union[Signature, CycleCover]):
        if isinstance(arg, Signature):
            n = arg.k
            edges = ((x // 2, y // 2) for x, y in enumerate(arg.matching) if y // 2 - x // 2 > 1)
        elif isinstance(arg, CycleCover):
            n = arg.n
            edges = ((min(x, y), max(x, y)) for x, y in enumerate(arg.permutation) if abs(x - y) > 1)
        else:
            raise TypeError('Expected Signature or CycleCover')
        self.n = n
        self.edges = sorted(set(edges))

    def __repr__(self):
        return f'DependencyGraph({self.n!r}, {self.edges!r})'

    def __format__(self, spec):
        if spec == '':
            return str(self)
        if spec == 'legacy':
            # This format shall become obsolete after C++ code cleanup.
            line = chain((self.n, len(self.edges)), (v + 1 for v in chain.from_iterable(self.edges)))
            return ' '.join(map(str, line))
        if spec == 'dimacs':
            def lines():
                yield f'p tw {self.n} {self.n - 1 + len(self.edges)}'
                for i in range(1, self.n):
                    yield f'{i} {i + 1}'
                for x, y in self.edges:
                    yield f'{x + 1} {y + 1}'
            return '\n'.join(lines())
        else:
            raise ValueError('Invalid format specifier')

    def __eq__(self, other):
        return (self.n, self.edges) == (other.n, other.edges)

    def __lt__(self, other):
        return (self.n, self.edges) < (other.n, other.edges)


class Decomposition:
    """A tree decomposition rooted at bag 0"""
    def __init__(self, dimacs: str):
        """Creates a Decomposition based on text input in DIMACS format"""
        line = iter(l for l in dimacs.splitlines() if l and l[0] != 'c')
        n, max_bag = map(int, next(line).split()[2:4])
        bags = [None] * n
        for _ in range(n):
            bag_id, *bag = map(lambda v: int(v) - 1, next(line).split()[1:])
            bags[bag_id] = set(bag)
        edges = [[] for _ in range(n)]
        for _ in range(n - 1):
            x, y = map(lambda v: int(v) - 1, next(line).split())
            edges[x].append(y)
            edges[y].append(x)

        def root_in(current):
            for child in edges[current]:
                edges[child].remove(current)
                root_in(child)
        root_in(0)

        self.tw = max_bag - 1
        self.bags = bags
        self.children = edges


class NiceDecomposition:
    """A nice tree decomposition in reverse Polish notation format."""
    def __init__(self, d: Decomposition):
        types = []
        nodes = []

        def transform(source, target):
            forget = source - target
            introduce = target - source
            types.extend('F' * len(forget) + 'I' * len(introduce))
            nodes.extend(chain(forget, introduce))

        def dfs(current):
            children = d.children[current]
            if children:
                for child in children:
                    dfs(child)
                    transform(d.bags[child], d.bags[current])
                types.extend('J' * (len(children) - 1))
            else:
                types.append('L')
                transform(set(), d.bags[current])

        dfs(0)
        transform(d.bags[0], set())

        self.tw = d.tw
        self.types = ''.join(types)
        self.nodes = nodes

    def __repr__(self):
        return f'NiceDecomposition({self.types!r}, {self.nodes!r})'

    def __format__(self, spec):
        if spec == '':
            return str(self)
        elif spec == 'text':
            return '{} {}'.format(self.types, ' '.join(map(str, self.nodes)))
        elif spec == 'legacy':
            # This format shall become obsolete after C++ code cleanup.
            def line():
                arg = iter(reversed(self.nodes))
                for t in reversed(self.types):
                    yield t
                    if t == 'I' or t == 'F':
                        yield str(next(arg) + 1)
            return ' '.join(line())
        else:
            raise ValueError('Invalid format specifier')


def decompose(graph: Union[DependencyGraph, str], *, path='tw-exact', debug=False) -> NiceDecomposition:
    if isinstance(graph, DependencyGraph):
        # Important: Larish ignores the last line if it doesn't end with newline.
        graph = f'{graph:dimacs}\n'
    elif not isinstance(graph, str):
        raise TypeError('Expected DependencyGraph or str.')
    proc = Popen([path], stdin=PIPE, stdout=PIPE, stderr=PIPE if debug else DEVNULL, universal_newlines=True)
    stdout, stderr = proc.communicate(graph)
    if proc.returncode != 0:
        raise ChildProcessError(f'Decomposer process exited with {proc.returncode}')
    result = NiceDecomposition(Decomposition(stdout))
    if debug:
        result.stdout = stdout
        result.stderr = stderr
    return result


def gen_library(k):
    assert k >= 2
    dep = [g for g, _ in groupby(sorted(DependencyGraph(cover) for cover in cycle_covers(k)))]
    info(f'Generated {len(dep)} unique dependency graphs for k = {k}. Decomposing...')
    for g in dep:
        g.decomposition = decompose(g)
        g.signatures = []
    info(f'Done decomposing. Generating signatures...')
    for s in signatures(k):
        dep[bisect_left(dep, DependencyGraph(s))].signatures.append(s)
    info(f'Generated {sum(len(g.signatures) for g in dep)} signatures.')
    lib = [[] for _ in range(k)]
    for g in dep:
        lib[g.decomposition.tw].append(g)
    return lib


def count_nice(lib, k=None, tw=None):
    """Count signatures with better de Berg exponent than dynamic exponent."""
    k_range = range(2, len(lib)) if k is None else (k,)
    tw_range = (lambda k: range(1, k)) if tw is None else (lambda _: (tw, ))
    nice, almost = [], []
    for k in k_range:
        for tw in tw_range(k):
            for graph in lib[k][tw]:
                for sig in graph.signatures:
                    sig.tw = tw
                    de_berg = sig.de_berg_exponent()
                    if de_berg < tw + 1:
                        nice.append(sig)
                    elif de_berg == tw + 1:
                        almost.append(sig)
    print(f'{len(nice)} nice and {len(almost)} almost nice signatures.')
    return nice, almost


def de_berg_table(lib):
    size = len(lib)
    table = [[[] for _ in range(size)] for _ in range(size)]
    for k in range(2, size):
        for tw in range(1, k):
            for graph in lib[k][tw]:
                for sig in graph.signatures:
                    sig.tw = tw
                    table[k][sig.de_berg_exponent()].append(sig)
    return table


def min_table(lib):
    size = len(lib)
    table = [[[] for _ in range(size)] for _ in range(size)]
    for k in range(2, size):
        for tw in range(1, k):
            for graph in lib[k][tw]:
                for sig in graph.signatures:
                    exp = min(tw + 1, sig.de_berg_exponent())
                    table[k][exp].append(sig)
    return table


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
