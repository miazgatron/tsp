#include <decomposition.h>

#include <sstream>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace kopt {
namespace {

TEST(Decomposition, InputOutput) {
  std::istringstream input("J F 1 I 1 L L");
  std::ostringstream output;
  Decomposition::Ptr decomposition;
  input >> decomposition;
  output << decomposition;
  EXPECT_EQ(input.str(), output.str());
}

TEST(Decomposition, Output) {
  auto decomposition = Decomposition::Join(
      Decomposition::Forget(ModEdge(1),
          Decomposition::Introduce(ModEdge(1),
              Decomposition::Leaf())),
      Decomposition::Leaf());
  std::ostringstream output;
  output << decomposition;
  EXPECT_EQ(output.str(), "J F 1 I 1 L L");
}

class MockVisitor {
 public:
  using Result = int;

  MOCK_CONST_METHOD0(Leaf, Result());
  MOCK_CONST_METHOD2(Introduce, Result(ModEdge, Result));
  MOCK_CONST_METHOD2(Forget, Result(ModEdge, Result));
  MOCK_CONST_METHOD2(Join, Result(Result, Result));
};

TEST(Decomposition, Dfs) {
  auto decomposition = Decomposition::Join(
      Decomposition::Forget(ModEdge(1),
          Decomposition::Introduce(ModEdge(1),
              Decomposition::Leaf())),
      Decomposition::Leaf());

    using testing::Return;
  MockVisitor visitor;
  EXPECT_CALL(visitor, Leaf()).WillRepeatedly(Return(1));
  EXPECT_CALL(visitor, Introduce(ModEdge(1), 1)).WillOnce(Return(2));
  EXPECT_CALL(visitor, Forget(ModEdge(1), 2)).WillOnce(Return(3));
  EXPECT_CALL(visitor, Join(3, 1)).WillOnce(Return(4));
  EXPECT_EQ(decomposition->Dfs(visitor), 4);
}

}  // namespace
}  // namespace kopt
