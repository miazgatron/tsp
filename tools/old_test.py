#!/usr/bin/env python3
import logging
import os
import random
import subprocess
from argparse import ArgumentParser
from collections import namedtuple
from logging import info
from math import sqrt
from resource import setrlimit, RLIMIT_CPU
from tempfile import NamedTemporaryFile
from typing import List, Optional


def parse_args(args=None):
    parser = ArgumentParser()
    parser.add_argument('-v', '--verbose', action='count', default=0, help='increase log verbosity (default: 0)')
    parser.add_argument('--name', default='kopt', help='name of executable to build and run (default: kopt)')
    parser.add_argument('--algo', default='clever', help='algorithm name to pass to solver (default: clever)')
    parser.add_argument('--cpu', metavar='NUM', type=int, default=2,
                        help='maximum CPU time in seconds per test (default: 2)',)
    parser.add_argument('--min-k', metavar='NUM', type=int, help='minimum k to run tests with')
    parser.add_argument('--max-k', metavar='NUM', type=int, help='maximum k to run tests with')
    parser.add_argument('test', metavar='TEST', choices=('validity', 'limits'), nargs='?', default='validity',
                        help='type of test to run (validity or limits, default: validity)')
    args = parser.parse_args(args)
    if args.min_k is None:
        args.min_k = 2 if args.test == 'validity' else 3
    if args.max_k is None:
        args.max_k = 5 if args.test == 'validity' else 6
    return args


ProcessTime = namedtuple('Time', 'elapsed user system')
FinishedProcess = namedtuple('Process', 'status stdout stderr time')

default = parse_args([])


class TestError(Exception):
    pass


def chdir_project_root():
    root = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
    os.chdir(root)


def get_solver(name=default.name, cpu_limit=default.cpu, algorithm=None):
    try:
        subprocess.run(['make', name], cwd='build/release', stdout=subprocess.DEVNULL, check=True)
    except subprocess.CalledProcessError as e:
        raise TestError(f'Failed to make solver {name}') from e

    def solver(stdin, args=None):
        cmd = [f'build/release/{name}']
        if args is not None:
            cmd += args
        if algorithm is not None:
            cmd.append(f'--algorithm={algorithm}')
        return time_process(cmd, stdin=stdin, cpu_limit=cpu_limit)
    return solver


def get_tests():
    tests = os.listdir('tests/t12')
    tests.sort()
    for test_name in tests:
        with open(os.path.join('tests/t12', test_name)) as f:
            yield TestData.read(parse_ints(f), name=test_name)


def time_process(args, stdin=None, cpu_limit=None):
    assert cpu_limit is None or cpu_limit > 0
    with NamedTemporaryFile('r') as tmp:
        time_args = ['time', '--quiet', '--output={}'.format(tmp.name), '--format=%e %U %S']
        proc = subprocess.Popen(
            time_args + args, universal_newlines=True,
            preexec_fn=(lambda: setrlimit(RLIMIT_CPU, (cpu_limit, cpu_limit))) if cpu_limit else None,
            stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = proc.communicate(stdin)
        # The first line of `time` output can be a warning message; the results are in the last line.
        elapsed, user, system = tmp.readlines()[-1].split()
    return FinishedProcess(status=proc.returncode, stdout=stdout, stderr=stderr,
                           time=ProcessTime(elapsed=float(elapsed), user=float(user), system=float(system)))


class Graph:
    """An undirected graph without loops"""
    def __init__(self, n):
        self.n = n
        self.__weight = [None for _ in range(n * (n - 1) // 2)]

    @staticmethod
    def read(data):
        n = data[0][0]
        graph = Graph(n)
        for i in range(1, n):
            for j in range(i):
                graph[i, j] = data[i][j]
        return graph

    @staticmethod
    def generate(n, *, precision=3, seed=None):
        assert precision >= 1
        if seed is not None:
            random.seed(seed)
        sample = random.sample(range(n * n), n)
        random.shuffle(sample)
        points = tuple((x // n, x % n) for x in sample)

        graph = Graph(n)
        for i in range(1, n):
            x1, y1 = points[i]
            for j in range(i):
                x2, y2 = points[j]
                graph[i, j] = round(10**precision * sqrt((x1 - x2)**2 + (y1 - y2)**2))
        return graph

    def __getitem__(self, edge):
        return self.__weight[self.__index(edge)]

    def __setitem__(self, edge, weight):
        self.__weight[self.__index(edge)] = weight

    def __str__(self):
        def lines():
            yield f'{self.n}'
            for i in range(1, self.n):
                yield ' '.join(str(self[i, j]) for j in range(i))
        return '\n'.join(lines())

    def __index(self, edge):
        x, y = edge
        if x < y:
            x, y = y, x
        assert self.n > x > y >= 0
        return x * (x - 1) // 2 + y


class Input:
    def __init__(self, k: int, graph: Graph):
        self.k = k
        self.graph = graph

    @staticmethod
    def read(data):
        return Input(data[0][0], Graph.read(data[1:]))

    @staticmethod
    def generate(k, n):
        return Input(k, Graph.generate(n))

    def __str__(self):
        return f'{self.k}\n{self.graph}'


class Output:
    def __init__(self, cycle: List[int], weight: Optional[int] = None, graph: Optional[Graph] = None):
        if weight is None:
            self.weight = sum(graph[cycle[i - 1], cycle[i]] for i in range(graph.n))
        else:
            self.weight = weight
        self.cycle = cycle
        self.changes = sum(abs(cycle[i - 1] - cycle[i]) not in (1, len(cycle) - 1) for i in range(len(cycle)))

    @staticmethod
    def read(data):
        return Output(list(data[1]), data[0][0])

    def __eq__(self, other):
        return (self.weight, self.cycle) == (other.weight, other.cycle)

    def __str__(self):
        cycle = ' '.join(map(str, self.cycle))
        return f'{self.weight}\n{cycle}'


class TestData:
    def __init__(self, graph: Graph, cycles: List[List[int]], name=None):
        self.graph = graph
        self.cycles = cycles
        self.name = name

    @staticmethod
    def read(data, **kwargs):
        n = data[0][0]
        graph = Graph.read(data[0:n])
        cycles = [list(data[i]) for i in range(n, 2 * n - 2)]  # The best cycle for each k = 2, ..., n.
        return TestData(graph, cycles, **kwargs)

    def input(self, k):
        return Input(k, self.graph)

    def output(self, k):
        return Output(self.cycles[k - 2], graph=self.graph)


def parse_ints(data):
    if isinstance(data, str):
        data = data.splitlines()
    return tuple(tuple(int(x) for x in line.split()) for line in data)


class ValidityTester:
    def __init__(self, test, k):
        self.test = test
        self.k = k

    def run(self, solver):
        process = solver(f'{self.test.input(self.k)}\n')
        if process.status != 0:
            raise self.error(f'Solver exited with status {process.status}')
        self.verify(Output.read(parse_ints(process.stdout)))
        info(f'Test {self.test.name} passed for k = {self.k}')

    def verify(self, actual: Output):
        expected = self.test.output(self.k)
        # Some (signature, embedding) pairs might yield a move with less changes than the signature has.
        if actual.changes < self.k and actual.weight < expected.weight:
            expected = self.test.output(actual.changes)
        if actual != expected:
            raise self.error(f'Invalid output:\n# expected\n{expected}\n# actual\n{actual}')

    def error(self, msg):
        return TestError(f'Failed at {self.test.name}, k = {self.k}: {msg}')


def find_max_n(k, solver):
    def fits_in_limit(n):
        proc = solver(f'{Input(k, Graph.generate(n))}\n')
        info(f'Test for k = {k}, n = {n} finished with status {proc.status}')
        return proc.status == 0

    low_n = high_n = k
    while fits_in_limit(high_n):
        low_n = high_n
        high_n *= 2
    while low_n < high_n:
        mid_n = (low_n + high_n + 1) // 2
        if fits_in_limit(mid_n):
            low_n = mid_n
        else:
            high_n = mid_n - 1

    return low_n


def main(args=default):
    v = args.verbose
    loglevel = logging.WARNING if v <= 0 else logging.INFO if v <= 1 else logging.DEBUG
    logging.basicConfig(level=loglevel)
    chdir_project_root()
    solver = get_solver(name=args.name, cpu_limit=args.cpu, algorithm=args.algo)
    print(f'Time limit per test is {args.cpu} s')
    if args.test == 'validity':
        try:
            num = 0
            for test in get_tests():
                num += 1
                for k in range(args.min_k, args.max_k + 1):
                    ValidityTester(test, k).run(solver)
        except Exception:
            raise
        else:
            print(f'All {num} tests passed.')
    elif args.test == 'limits':
        for k in range(args.min_k, args.max_k + 1):
            n = find_max_n(k, solver)
            print(f'For k = {k} maximum n is {n}')
    logging.shutdown()


if __name__ == '__main__':
    main(parse_args())
