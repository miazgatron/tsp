#!/usr/bin/env python3
from itertools import permutations
from random import randint


class RandomGraph:
    def __init__(self, n):
        mx = (2**31 - 1) // n  # Ensure cycle weight fits in a signed 32-bit integer.
        self.n = n
        self.e = [randint(1, mx) for _ in range(n * (n - 1) // 2)]

    def __getitem__(self, edge):
        x, y = edge
        if x < y:
            x, y = y, x
        assert 0 <= y <= x < self.n
        return self.e[x * (x - 1) // 2 + y] if x != y else None

    def __str__(self):
        def lines():
            yield f'{self.n}'
            for i in range(1, self.n):
                yield ' '.join(str(self[i, j]) for j in range(i))
            try:
                cycle = self.cycle
            except AttributeError:
                pass
            else:
                for i in range(2, self.n + 1):
                    yield ' '.join(map(str, cycle[i]))
        return '\n'.join(lines())


def cycles(n):
    for p in permutations(range(1, n)):
        if p[0] < p[-1]:
            yield (0, *p)


def find_kmoves(g):
    n = g.n
    best = [None] * (n + 1)
    count = [None] * (n + 1)
    cycle = [None] * (n + 1)
    for c in cycles(n):
        dif = sum(abs(c[i - 1] - c[i]) not in (1, n - 1) for i in range(n))
        weight = sum(g[c[i - 1], c[i]] for i in range(n))
        if best[dif] is None or weight < best[dif]:
            best[dif] = weight
            count[dif] = 1
            cycle[dif] = c
        elif weight == best[dif]:
            count[dif] += 1
    g.best = best
    g.count = count
    g.cycle = cycle
    return g
